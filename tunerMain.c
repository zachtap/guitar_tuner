/*
Guitar Tuner Module Written by Zachary Tapia 
 */
#include "simpletools.h"                      // Include simple tools
#include "adcDCpropab.h"
float signalT1;                               //signal storage
float signalT2;                               //storage for signal
int T1;                                       //timer storage
int T2;
int tSync; 
int bgTicks;                                  //background timer ticks
int notePeakCount[] = {82, 110, 146, 196, 247, 330};//start, E A D G B e
int notePause[] = {8, 6, 4, 4, 4, 2};         //pause to allow peak to pass
float noteThreshold[] = {2.7, 2.65, 2.7, 2.63, 2.6, 2.6};
int note;
int tone;
int peakCount;
int button;

void bgTimer();//background Timer. 
void getTone();//check current note for tone
void flow();//controls note state

int main()                                    // Main function
{
  adc_init(21,20,19,18);                      //initialize ADC converter #3 to read Signal
  bgTicks = 0;
  note = 0;                                //set timer to zero
  cog_run(bgTimer, 128);                      //start timer
  cog_run(flow, 128);                         //polls button to change state
  cog_run(getTone, 128);                      //constantly look at note for tone


  while(1){//be checking and outputting tone status pins 15, 14, 13, ---12---, 11, 10, 9, 
    if(tone < -3){high(9); low(10); low(11);low(12);low(13);low(14);low(15);}; 
    if(tone >= -3 && tone < -1 ){low(9); high(10); low(11);low(12);low(13);low(14);low(15);};  
    if(tone >= -1 && tone < 0 ){low(9); low(10); high(11);low(12);low(13);low(14);low(15);};  
    if(tone == 0 ){low(9); low(10); low(11); high(12);low(13);low(14);low(15);};  
    if(tone <= 1 && tone > 0 ){low(9); low(10); low(11); low(12);high(13);low(14);low(15);};
    if(tone <= 3 && tone > 1 ){low(9); low(10); low(11); low(12);low(13);high(14);low(15);};
    if(tone > 3 ){low(9); low(10); low(11); low(12);low(13);low(14);high(15);};      
    
    if(note == 6){
      print("tuned. do an interrupt and i2c to rpi");//create a signal for this. variable. when
      //this variable is high, send the signal i2c or whatever.
      //figure out how to set up the propeller as slave. rpi as master. get signal from propeller.
      high(7);//send signal if tuned - replace with i2c protocol
      pause(5000);
      low(7);//turn off signal
      pause(1000);
    };      
    
    
  }//1st while(1)         
}//main

void flow(){
 while(1){
   high(note+1);                      //push button to step through all notes
   int button = input(0);
    if(button == 1){                  
      if(note < 6){
        note = note + 1;
      } else {
       note = 0;
      }  
      low(1); low(2); low(3); low(4); low(5); low(6);//LED Display note
      high(note+1);
      pause(250);                   //debounce the switch
    };//if button
 }    
}  
void bgTimer(){                   
  tSync = CNT;                    //get clock time
  while(1){
    tSync = tSync + CLKFREQ/50000;//get ready to wait for 20 us
    waitcnt(tSync);               //wait
    bgTicks = bgTicks + 1;//each digit is 1/10 millisecond while waiting
  }      
}; //bgTimer

void getTone(){
  while(1){
    signalT1 = adc_volts(3);//check audio
      if(signalT1 >= noteThreshold[note] ){//if audio is loud enough
        pause(notePause[note]);//start counting peaks. pause long enough to let the peak hump pass
        T1 = bgTicks;         //start the timer
        while(bgTicks < T1 + 50000){//for 1 second, cound peaks
          signalT1 = adc_volts(3);
          if(signalT1 >= noteThreshold[note]){
            peakCount = peakCount + 1;
            pause(notePause[note]);
          }            
      }   
      tone = peakCount - notePeakCount[note];//compare tone
      peakCount = 0;        
    }
  }//while loopo    
};//getTone  

