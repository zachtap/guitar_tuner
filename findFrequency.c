/*
 This can probably be the entire module. 
 
 **/
#include "simpletools.h"                      // Include simple tools
#include "adcDCpropab.h"
float signalT1;                               //signal storage
float signalT2;                               //storage for signal
int T1;                                       //timer storage
int T2;
float Vthreshold;                             //set up as array 
int tSync; 
int bgTicks;                                  //background timer ticks
int peakCount;
float notes[] = {16.488, 55, 34 , 30.6 , 32.32 , 21.21};;
void bgTimer();

int main()                                    // Main function
{
  adc_init(21,20,19,18);                      //initialize ADC converter #3 to read Signal
  Vthreshold = 2.7;                           //when the input signal reaches this, play has started. array for each note? some are louder
  bgTicks = 0;                                //bgTimer. each bgTick is 1/10 millisecond
  peakCount = 0;                              //
  cog_run(bgTimer, 128);                      //

  while(1){
    signalT1 = adc_volts(3);                  //listen for signal
    if(signalT1 >= Vthreshold){               //if signal is larger than threshold
      T1 = bgTicks;                           //record the time the signal goes high
      peakCount ++;                           //increment the peak Count
      pause(5);                               //pause to let the peak overshoot pass. set up an array with appropriate pause times for each signal
      while(bgTicks < T1 + 9090){             //count Peaks until the bgTimer passes 10 periods. set up an array with all note values. 9090 ticks = 9090*1/10(ms) = 100 periods A
        signalT1 = adc_volts(3);              //get new signal reading
        if(signalT1 >= Vthreshold){           //is the new signal a peak?
          peakCount++;                        //YES? increment the peak.
          pause(5);                           //pause to let the overshoot pass.          
        }  //if signal > Vthreshold (2nd)        
      }//while timer multiple of T        
      print("peak count: %d\n",peakCount);
      signalT1 = adc_volts(3);//reset the signal
      peakCount = 0;//reset count
    }//if signal>Vthreshold        
  }//1st while(1)          
}//main


void bgTimer(){
  tSync = CNT;
  while(1){
    tSync = tSync + CLKFREQ/10000;
    waitcnt(tSync);
    bgTicks = bgTicks + 1;//each digit is 1/10 millisecond
  }    
    
};

//4/27. This version seems to work. 
//peaks are counted starting from when the signal is read above Vthreshold
//a timer is then started and the number of peaks is recorded then output